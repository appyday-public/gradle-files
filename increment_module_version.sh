# arguments
GRADLE_PATH=$1
GRADLE_FIELD=$2

if [ -z "${GRADLE_PATH}" ]; then
    echo "GRADLE_PATH is unset or set to the empty string"
    exit 1
fi

if [ -z "${GRADLE_FIELD}" ]; then
    echo "GRADLE_FIELD is unset or set to the empty string"
    exit 1
fi

# variables
VERSION_TMP=$(grep $GRADLE_FIELD $GRADLE_PATH | awk '{print $3;}')    # get value versionName"0.1.0"
VERSION=$(echo $VERSION_TMP | sed -e 's/^"//'  -e 's/"$//')  # remove quotes 0.1.0
incrementVersion=$(echo $VERSION + 0.01 | bc -l)
quotes='"'
# replace
sed -ie "/${GRADLE_FIELD}/s/= .*/= ${quotes}${incrementVersion}${quotes}/" $GRADLE_PATH
echo next $GRADLE_FIELD: $incrementVersion